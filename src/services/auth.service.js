import axios from "axios";

const API_URL = `http://54.155.217.247:8888/api/auth`;

const register = async (username, password) => {
  try {
    const response = await axios.post(`${API_URL}/signup`, {
      username,
      password,
    }, { timeout: 4000 });
    const data = await response.data;
    return data;
  } catch (error) {
    window.location.href = "http://54.155.217.247:8888/signup/error";
  }
};

const login = async (username, password) => {
  try {
    const response = await axios.post(`${API_URL}/signin`, {
      username,
      password,
    }, { timeout: 4000 });
    const data = await response.data;
    if (data.accessToken) {
      localStorage.setItem(`user`, JSON.stringify(response.data));
    }
    return data;
  } catch (error) {
    window.location.href = "http://54.155.217.247:8888/signin/error";
  }
};

const searchJourneys = async (
  startAirport,
  endAirport,
  currentLat,
  currentLon
) => {
  try {
    const response = await axios.post(`${API_URL}/alljourneys`, {
      airportRequest: {
        startAirport,
        endAirport,
      },
      currentLat,
      currentLon,
    });

    let data = await response.data;

    if(data.departureCity.flights.length === 0){
    //   // sending back an object with a messsage key
      data = {message: "DREAM ON, TRY AGAIN IN 2021.. "} ;
    }

    // try catch: if data[path of where it's coming from - e.g. data.departurecity.flight.length === 0 ]
    // make data equal to a string with an error message. ]
// check file paths
    return data;
  } catch (error) {
    let data = {message: "Oops! Looks like something went wrong on our end. Try again..."} ;;
    return data;
  }
};

const logout = () => {
  localStorage.removeItem(`user`);
};

const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem(`user`));
};

export default {
  register,
  login,
  logout,
  getCurrentUser,
  searchJourneys,
};
