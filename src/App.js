import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AuthService from "./services/auth.service";
import Travel from './components/travel/Travel'
import Navbar from "./components/navbar/Navbar";
import Home from "./components/home/Home";
import Login from "./components/login/Login.jsx";
import Register from "./components/register/Register.jsx";
import NotFound from "./components/notFound/NotFound.jsx";
import Loader from "./components/loader/loader"

import "./App.css";

const App = () => {
  const [currentUser, setCurrentUser] = useState(undefined);
  const [currentLat, setCurrentLat] = useState(51.5321);
  const [currentLon, setCurrentLon] = useState(0.0308);
  const [searchClicked, setSearchClicked ] = useState(false); 
  const [journeys, setJourneys] = useState(null);
  const [loading, setLoading] = useState(false);
  const [startCity, setStartCity] = useState("");
  const [endCity, setEndCity] = useState("");

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      setCurrentUser(user);
    }
  }, []);

  const getUser = () => {
    const user = AuthService.getCurrentUser();
    if (user) {
      setCurrentUser(user);
    } else {
      setCurrentUser(undefined);
    }
  };

  navigator.geolocation.getCurrentPosition(position => {
    setCurrentLat(position.coords.latitude);
    setCurrentLon(position.coords.longitude);
  });

  const updateSearchClicked = (bool) => {
    setSearchClicked(bool);
  }


  const updateJourneys = (journey) => setJourneys(journey)
  const updateLoading = (bool) => setLoading(bool)
  const updateStartCity = (city) => setStartCity(city);
  const updateEndCity = (city) => setEndCity(city);

  return (
    <Router>
      <div>
        <Navbar currentUser={currentUser} getUser={getUser} />
        {loading ?
          <div className="loaderbackground"><Loader /></div> :
          <div>
            {currentUser ? (
              <Switch>
                <Route exact path="/home">
                  <Home
                    searchClicked={searchClicked}
                    currentUser={currentUser}
                    currentLat={currentLat}
                    currentLon={currentLon}
                    startCity={startCity}
                    endCity={endCity}
                    updateStartCity={updateStartCity}
                    updateEndCity={updateEndCity}
                    updateJourneys={updateJourneys}
                    updateLoading={updateLoading}
                    updateSearchClicked={updateSearchClicked}
                  />
                </Route>
                <Route exact path="/travel">
                  <Travel
                    searchClicked={searchClicked}
                    journeys={journeys}
                    currentLat={currentLat}
                    currentLon={currentLon}
                    startCity={startCity}
                    endCity={endCity}
                    updateStartCity={updateStartCity}
                    updateEndCity={updateEndCity}
                    updateJourneys={updateJourneys}
                    updateLoading={updateLoading}
                    updateSearchClicked={updateSearchClicked}
                  />
                </Route>
                <Route exact path="/:notfound">
                  <NotFound />
                </Route>
              </Switch>
            ) : (
                <Switch>
                  <Route exact path="/">
                    <Login
                      getUser={getUser}
                      updateLoading={updateLoading}
                    />
                  </Route>
                  <Route exact path="/register">
                    <Register
                      getUser={getUser}
                      updateLoading={updateLoading}
                    />
                  </Route>
                  <Route>
                    <NotFound />
                  </Route>
                </Switch>
              )}
          </div>
        }
      </div>
    </Router>
  );
};

export default App;
