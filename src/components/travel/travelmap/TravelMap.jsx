import React, { useEffect } from 'react';
import './TravelMap.css';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

function TravelMap({ journeys, flightId, drivingId }) {

    let flightRenderId = flightId ? flightId - 1 : 0;
    let drivingRenderId = drivingId ? drivingId - 1 : 0;

    //driving
    let drivingRouteCoordinates = [];
    let flightLegsCoordinates = [];
    let centreCoordinates = [0, 0];

    if(journeys && journeys.departureCity) {
        // setting driving coordinates
        journeys.departureCity.flights[flightRenderId].drivingRoutes[drivingRenderId].steps.forEach(step => {
            return drivingRouteCoordinates = [...drivingRouteCoordinates, ...step.coordinates];
        });

        //setting flight coordinates
        journeys.departureCity.flights[flightRenderId].flightLegs.forEach((flightLeg, index) => {
            if(index === 0) {
                flightLegsCoordinates = [...flightLegsCoordinates, [flightLeg.departureLon, flightLeg.departureLat]];
            } 
            flightLegsCoordinates = [...flightLegsCoordinates, [flightLeg.arrivalLon, flightLeg.arrivalLat]];
        })
    }
    //flights

    // driving + flights 
    
    var MapboxReady = [];
    if(!journeys || !journeys.departureCity) {
        MapboxReady = [[-122.49378204345702, 37.83368330777276], [51.470020, -0.454295]];
    } else {
        MapboxReady = drivingRouteCoordinates.concat(flightLegsCoordinates);
    }

    var bounds = MapboxReady.reduce(function (bounds, coord) {
        return bounds.extend(coord);
    }, new mapboxgl.LngLatBounds(MapboxReady[0], MapboxReady[0]));
    
    if(journeys && journeys.departureCity) {
        // setting centre coordinates
        let centreLat = (bounds._sw.lat + bounds._ne.lat) / 2;
        let centreLon = (bounds._ne.lng + bounds._sw.lng) / 2;
        centreCoordinates = [centreLon, centreLat];
    }

    useEffect(() => {
        // mapbox access token
        mapboxgl.accessToken = 'pk.eyJ1IjoibGV4amFtZXMwNiIsImEiOiJja2ZwYnJ2bW8wMndxMnhvZjY3bnpqeDdlIn0._WIsBZqmlrlS9FA_Vbbr9g';
        
        const map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/outdoors-v11', // style URL
            center: centreCoordinates,    // ** ADD CURRENT LOCATION PROP HERE 
            zoom: 1 // starting zoom
        });

        map.on('load', function () {

            map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': [
                        {
                            'type': 'Feature',
                            'properties': {
                                'color': '#495057' // GREY
                            },
                            'geometry': {
                                'type': 'LineString',
                                'coordinates': drivingRouteCoordinates
                            }
                        },
                        {
                            'type': 'Feature',
                            'properties': {
                                'color': '#ee6c4d' // ORANGE
                            },
                            'geometry': {
                                'type': 'LineString',
                                'coordinates': flightLegsCoordinates
                            }
                        }
                    ]
                }
            });

            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': ['get', 'color'],
                    'line-width': 3
                }
            });

            map.addControl(new mapboxgl.NavigationControl());

            map.addControl(new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            }));

            map.fitBounds(bounds, {
                padding: 20
            });
        });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [journeys, drivingId]);

    // , flightLegsCoordinates, drivingRouteCoordinates, centreCoordinates, bounds
    // }, [journeys, MapboxReady, drivingArray, flightLegsArray]);

    return (
        <>
            <div id='map'> </div>
        </>
    );
}

export default TravelMap;
