import React from "react";
import PropTypes from "prop-types";
import TravelIcon from './TravelIcon.component';
import ArrowSVG from "../../../common/icon/arrow";

import "./TravelCard.styles.scss";

const TravelCard = ({ flight, drivingRoute, updateShowRoute, currentLocationWeather }) => {

    // flight class, bits of the flight - flight.flightID, now flightId
    let {
        flightId,
        flightLegs,
        price,
        fly_duration,
        deep_link
    } = flight;

    // if you have an object 
    let {
        drivingRouteId, //Driving route class
    } = drivingRoute; 

    let flightLegsAsAString = ``;
    flightLegs && flightLegs.map((flightLeg, index) => {
        if(index !== 0) { 
            return flightLegsAsAString = `${flightLegsAsAString}, Fly to ${flightLeg.departureAirportName}`; 
        }
        return null;
    });

    let journeyString = `Drive to ${flightLegs[0].departureAirportName}${flightLegsAsAString}, Fly to ${flightLegs[flightLegs.length - 1].arrivalAirportName}`;
    // conversion of price once the euro sign has been removed from the travelPrice
    price = Math.round(price * 0.91);

    let weatherConditions = [{modeOfTransport: "driving", weather: currentLocationWeather.condition}];

    let travelIcons = [...weatherConditions];

    flightLegs && flightLegs.forEach(flightLeg => {
        return travelIcons = [...travelIcons, {
            modeOfTransport: "flying", 
            weather: flightLeg.departureWeather.condition
        }]
    })

    const GetTravelIcons = () => { 
        return travelIcons.map(({ modeOfTransport, weather }, index) => {
        return (
                <TravelIcon 
                    key={index}
                    modeOfTransport={modeOfTransport} 
                    weather={weather} 
                    zIndex={travelIcons.length - index} />
            ); 
        });
    }

    return (
        <div className="travelCard">
            <div onClick={() => updateShowRoute(true, flightId, drivingRouteId, journeyString)}>
                <div className="travelInfo">
                    <h5 className="travelOption"> {journeyString}</h5>
                    <p className="travelTime">{fly_duration}</p>
                </div>
                <div className="iconBar">
                    <div className="travelIcons">
                        <GetTravelIcons />
                    </div>
                    <div className="selectCardIcon">
                        <p>£{price}</p>
                        <ArrowSVG />
                    </div>
                </div>
            </div>
            <div className="deep_link">
                <a href={`${deep_link}`} target="_blank" rel="noreferrer noopener">
                    Book your flight!
                </a>
            </div>
        </div>
    );
};

TravelCard.propTypes = {
    travelOption: PropTypes.string.isRequired,
    travelTime: PropTypes.string.isRequired,
};

TravelCard.defaultProps = {
    travelOption: "testOption",
    travelTime: "2h 54m",
};

export default TravelCard;
