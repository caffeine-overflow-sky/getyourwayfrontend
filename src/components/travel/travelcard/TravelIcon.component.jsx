import React from 'react';

import CarSVG from "../../../common/icon/car";
import PlaneSVG from "../../../common/icon/plane";

const TravelIcon = ({ modeOfTransport, zIndex }) => {

    let travelIconStyle = { backgroundColor: "black" };

    if (modeOfTransport === "driving") {
        travelIconStyle = { backgroundColor: "#495057", zIndex: zIndex };
    } else if (modeOfTransport === "flying") {
        travelIconStyle = { backgroundColor: "#ee6c4d", zIndex: zIndex };
    }

    const TransportIcon = ({ modeOfTransport }) => {
        switch (modeOfTransport) {
            case "driving":
                return <CarSVG />;
            case "flying":
                return <PlaneSVG />;
            default:
                return null;
        }
    };

    return (
        <div className="travelIcon" style={travelIconStyle}>
            <div className="travelIcon-Container" >
                <div className="travelMode">
                    <TransportIcon modeOfTransport={modeOfTransport} />
                </div>
            </div>
        </div>
    );
};

export default TravelIcon;