import React from 'react';
import Car from "../../../../common/icon/car";
import Plane from "../../../../common/icon/plane";
import Sun from "../../../../common/icon/sun";
import Cloud from "../../../../common/icon/cloudandsun";
import Rain from "../../../../common/icon/rain";

import "./Step.css";

const Step = ({ step }) => {

    const { location, instruction, weatherCondition, weatherTemp, modeOfTransport } = step;

    const showModeOfTransport = () => {
        switch(modeOfTransport) {
            case "driving":
                return <Car />;
            case "flying":
                return <Plane />;
            default:
                return null;
        }
    }

    const showWeatherIcon = () => {
        switch(weatherCondition) {
            case "Clouds":
            case "Mist":
            case "Smoke":
            case "Haze":
            case "Dust":
            case "Fog":
            case "Sand":
            case "Ash":
            case "Squall":
            case "Tornado":
            case "Snow":
                return <Cloud />;
            case "Clear":
                return <Sun />;
            case "Rain":
            case "Thunderstorm":
            case "Drizzle":
                return <Rain />;
            default:
                return <Sun />;
        }
    }

    let travelIconStyle;

    if (modeOfTransport === "driving") {
        travelIconStyle = { fill: "#495057", height: "30px", width: "30px", padding: "2px" };
    } else if (modeOfTransport === "flying") {
        travelIconStyle = { fill: "#ee6c4d",  height: "30px", width: "30px", padding: "2px" };
    }

    return (
        <div className="StepData pr-4 pl-4">
            <div className="d-flex flex-column">
                <div className="d-flex justify-content-between" >
                    <div className="d-flex align-items-center">
                        <p className="stepLocation m-0">
                            {location}
                        </p>
                    </div>
                    <div className="d-flex">
                        <div className="stepTravelIcons">
                            {showWeatherIcon()}
                        </div>
                        <div className="d-flex align-items-center pl-2">
                            <p className="m-0">
                                {(weatherTemp - 273).toPrecision(2)}°C
                            </p>
                        </div>
                    </div>
                </div>
                {
                    instruction && (
                        <div className="d-flex pr-2 pl-2">
                            <div className="stepConnection d-flex align-items-center">
                                <div className="stepTravelIcons m-2" style={travelIconStyle}>
                                    {showModeOfTransport()}
                                </div>
                            </div>
                            <div className="d-flex align-items-center">
                                <p className="stepInstruction text-align-center pl-2 m-0">
                                    {instruction}
                                </p>
                            </div>
                        </div>
                    )
                }
            </div>
        </div >
    );
}

export default Step;