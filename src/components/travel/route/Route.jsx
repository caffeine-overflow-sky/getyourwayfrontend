import React from 'react'
import './Route.css'
import ArrowSVG from "../../../common/icon/arrow";
import Step from "./step/Step";

const Route = ({ flight, journeyString, currentLocationWeather, updateShowRoute }) => {

    let { cityFrom, flyFrom, cityTo, flyTo, fly_duration, flightLegs } = flight;

    let stepsToDisplay = [];

    let drivingStep = {
        location: "Start", 
        instruction: `Drive to ${cityFrom} ${flyFrom} airport`,
        weatherTemp: currentLocationWeather.temp, 
        weatherCondition: currentLocationWeather.condition,
        modeOfTransport: "driving"
    };

    let lastFlightLegWeather = flightLegs[flightLegs.length - 1].arrivalWeather;

    let lastStep = {
        location: `Arrive at ${cityTo} ${flyTo} airport`, 
        weatherTemp: lastFlightLegWeather.temp, 
        weatherCondition: lastFlightLegWeather.condition,
        modeOfTransport: "flying"
    };

    let flightLegsToMap = flightLegs.reduce((flightLegsArray, flightLeg, index) => {

            let { 
                departureAirportName, 
                departureAirportCodeName, 
                departureTime, 
                arrivalTime, 
                arrivalAirportName, 
                arrivalAirportCodeName, 
                departureWeather
            } = flightLeg;

            departureTime = new Date(departureTime * 1000);
            arrivalTime = new Date(arrivalTime * 1000)

            let departureHours = departureTime.getHours();
            let departureMinutes = departureTime.getMinutes();

            if(departureHours < 10) departureHours = `0${departureHours}`;
            if(departureMinutes < 10) departureMinutes = `0${departureMinutes}`;

            departureTime = `${departureHours}:${departureMinutes}`;

            let arrivalHours = arrivalTime.getHours();
            let arrivalMinutes = arrivalTime.getMinutes();

            if(arrivalHours < 10) arrivalHours = `0${arrivalHours}`;
            if(arrivalMinutes < 10) arrivalMinutes = `0${arrivalMinutes}`;

            arrivalTime = `${arrivalHours}:${arrivalMinutes}`;

            if(index === 0) departureAirportCodeName = flyFrom;

            let flightLegDetails = {
                location: `${departureAirportName} ${departureAirportCodeName} (departs at: ${departureTime})`,
                instruction: `Fly to ${arrivalAirportName} ${arrivalAirportCodeName} (arrives at: ${arrivalTime})`,
                weatherCondition: departureWeather.condition,
                weatherTemp: departureWeather.temp,
                modeOfTransport: "flying"
            }

            return [...flightLegsArray, flightLegDetails]
        }, []
    );

    stepsToDisplay = [drivingStep, ...flightLegsToMap , lastStep];

    return (
        <div className="route d-flex flex-column">
            <div className="d-flex p-1">
                <div className="routeRedirectIcon" onClick={() => updateShowRoute(false)}>
                    <ArrowSVG />
                </div>
                <div className="d-flex flex-column">
                    <h5 className="routeOption">{journeyString}</h5>
                    <p>{fly_duration}</p>
                </div >
            </div>
            {stepsToDisplay.map((step, index) => {
                return (
                    <Step key={index} step={step} />
                )
            })}
        </div>
    )
}

export default Route;