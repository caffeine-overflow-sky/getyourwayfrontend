import React, { useState, useEffect } from 'react'
import Search from '../search/Search.jsx'
import TravelCard from './travelcard/TravelCard.jsx'
import TravelMap from './travelmap/TravelMap'
import Route from './route/Route.jsx';
import './Travel.css'

const Travel = (props) => {
    let {
        searchClicked,
        currentLat,
        currentLon,
        startCity,
        endCity,
        updateStartCity,
        updateEndCity,
        updateLoading,
        journeys,
        updateJourneys,
        updateSearchClicked
    } = props

    const [showRoute, setShowRoute] = useState(false);
    const [flightId, setFlightId] = useState(null);
    const [drivingId, setDrivingId] = useState(null);
    const [journeyString, setJourneyString] = useState(null);

    const updateShowRoute = (bool, flightId, drivingId, journeyString) => {
        setShowRoute(bool)
        setFlightId(flightId)
        setDrivingId(drivingId)
        setJourneyString(journeyString);
    }

// 2 VARIABLES -> travel options class and travel map class
// conditional render, check if the journeys instance is there if(!journeys){ CHANGE EM BECAUSE FALSY } THEN it changes 
// the classNames of the previously declared vars accordingly to journeys being NULL 

// classNames hence pull the CSS accordingly to the journeys value 

let travelOptionsStyling = "noSearchTravelOptions";

let travelMapStyling = "noSearchMap";

if(journeys && journeys.departureCity) {
    travelOptionsStyling = "journeyDataTravelOptions"
    travelMapStyling = "journeyDataMap"
}

    const TravelCards = () => {
        if (journeys && journeys.departureCity) {
            return journeys.departureCity.flights.map((flight, index) => {
                return <TravelCard
                    key={index}
                    // this is corresponding to the flight this driving route came from - two driving routes have the 
                    // same flight object passed to them
                    flight={flight}
                    drivingRoute={flight.drivingRoutes[0]}
                    updateShowRoute={updateShowRoute}
                    currentLocationWeather={journeys.weatherAtStartPoint}
                    />
            });
        }
    }

    const handleTravelOptionsColumn = () => {
        if(showRoute) {
            return (
                <Route
                flight={journeys.departureCity.flights[flightId - 1]}
                flightId={flightId}
                drivingId={drivingId}
                journeyString={journeyString}
                currentLocationWeather={journeys.weatherAtStartPoint}
                updateShowRoute={updateShowRoute} />
            );
        } else {
            return TravelCards();
        }
    }

    useEffect(() => {
    }, [showRoute])

    return (
        <>
            <div className="background">
                <Search
                    searchClicked={searchClicked}
                    journeys={journeys}
                    currentLat={currentLat}
                    currentLon={currentLon}
                    startCity={startCity}
                    endCity={endCity}
                    updateStartCity={updateStartCity}
                    updateEndCity={updateEndCity}
                    updateLoading={updateLoading}
                    updateJourneys={updateJourneys}
                    updateSearchClicked={updateSearchClicked}
                />
            </div>
            <div className="travelPage ">
                <div className="travelOptions background" id={travelOptionsStyling}>
                    {handleTravelOptionsColumn()}
                </div>
                <div className= {`travelMap background flex-fill`} id={travelMapStyling}>
                    <TravelMap
                        flightId={flightId}
                        drivingId={drivingId}
                        journeys={journeys}
                        updateLoading={updateLoading}
                    />
                </div>
            </div>
        </>
    )
}

export default Travel;