import React, { useState, useRef } from "react";
import { Link, useHistory } from "react-router-dom";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../../services/auth.service";
import ValidationServiceHelpers from "../../services/validation.serviceHelpers";
import "./Login.css";

const Login = ({ loading, updateLoading, getUser }) => {
  const form = useRef();
  const checkBtn = useRef();

  const [username, setUsername] = useState(``);
  const [password, setPassword] = useState(``);
  const [message, setMessage] = useState(``);

  let history = useHistory();

  const onChangeUsername = (e) => {
    const newUsername = e.target.value;
    setUsername(newUsername);
  };

  const onChangePassword = (e) => {
    const newPassword = e.target.value;
    setPassword(newPassword);
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    setMessage(``);
    updateLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      const login = await AuthService.login(username, password);
      if (localStorage.getItem("user")) {
        getUser();
        history.push(`/home`);
        window.location.reload();
      } else {
        console.dir(login);
        setMessage(login.error);
        updateLoading(false);
      }
    } else {
      updateLoading(false);
    }
  };

  return (
    <div className="section-login  col-md-12">
      <div className="overlay d-flex flex-column">
        <div className="card card-container flex-fill">
          <div>
            <p className="medium-text text-center text-break">Welcome back</p>
            <p className="cssanimation hu__hu__ sequence heading-text text-center text-break">
              Ready for your next adventure?
              </p>
          </div>
          <Form onSubmit={handleLogin} ref={form}>
            <div className="form-group">
              <Input
                type="text"
                className="form-control"
                name="username"
                placeholder="Enter username"
                value={username}
                onChange={onChangeUsername}
                validations={[ValidationServiceHelpers.required]}
              />
            </div>
            <div className="form-group">
              <Input
                type="password"
                className="form-control"
                name="password"
                placeholder="Enter password"
                value={password}
                onChange={onChangePassword}
                validations={[ValidationServiceHelpers.required]}
              />
            </div>
            <div className="form-group">
              <button
                className="btn btn-block btn-secondary loginButton"
                disabled={loading}
                type="submit"
              >
                Login
                </button>
            </div>
            {message && (
              <div className="form-group">
                <div className="alert alert-danger" role="alert">
                  {message}
                </div>
              </div>
            )}
            <CheckButton style={{ display: "none" }} ref={checkBtn} />
          </Form>
        </div>
        <div className="card card-container flex-fill">
          <Link to="/register">
            <button
              className="btn btn-block btn-secondary registerButton"
              disabled={loading}
              type="submit"
            >
              Register
              </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Login;
