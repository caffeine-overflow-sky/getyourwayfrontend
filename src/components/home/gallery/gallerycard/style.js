import styled, { keyframes } from "styled-components"

const pulse = keyframes`
    0% {
        transform: scale(1);
    }
    50% {
        transform: scale(1.03);
    }
    100% {
        transform: scale(1);
    }
`

export const GalleryCardContainer = styled.div`
    background-image: ${props => `url(${require(`../../../../common/images/gallery/${props.img}`)})`};
    background-size: cover;
    background-position: center;
    min-width: 18.6rem;
    min-height: 18rem;
    margin: 0.5rem;
    box-shadow: 2px 2px 4px 2px lightgray;
    border-radius: 5px;

    cursor: pointer;

    :hover {
        animation: ${pulse} 3s infinite;
    }

    @media (max-width: 768px) {
        min-width: 12rem;
        min-height: 12rem;
        margin: 0.25rem
    }
    `