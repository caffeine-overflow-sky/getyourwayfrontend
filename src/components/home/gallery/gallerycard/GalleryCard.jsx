import React from "react";
import { useHistory } from "react-router-dom";
import AuthService from '../../../../services/auth.service'
import './GalleryCard.css';
import { GalleryCardContainer } from './style.js'

const GalleryCard = (props) => {
    let { name, image, startCity, endCity, updateStartCity, updateEndCity, updateJourneys, currentLat, currentLon, updateLoading } = props;

    let history = useHistory();

    const searchCities = async () => {
        updateStartCity(startCity)
        updateEndCity(endCity)
        try {
            updateLoading(true)
            const journeys = await AuthService.searchJourneys(startCity, endCity, currentLat, currentLon);

            journeys && updateLoading(false)
            updateJourneys(journeys);
            history.push("/travel")
        } catch (e) {
            updateLoading(false)
        }
    }
    return (
        <>
            <GalleryCardContainer colour="green" img={image} onClick={() => searchCities()}>
                <div className="galleryContent d-flex align-items-end ">
                    <h6 className="journeyName">{name}</h6>
                </div>
            </GalleryCardContainer>
        </>
    )
}

export default GalleryCard;


