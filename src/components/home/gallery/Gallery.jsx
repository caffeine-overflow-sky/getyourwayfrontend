import React from "react"
import GalleryCard from "./gallerycard/GalleryCard";
import Data from "./galleryData.json";
import './Gallery.css'

const Gallery = ({ updateStartCity, updateEndCity, updateJourneys, currentLat, currentLon, updateLoading }) => {
    return (
        <div id="gallery" className="gallery p-2">
            <hr />
            <h3 className="text-center">Featured trips</h3>
            <hr />
            <div className="gallerygrid">
                {
                    Data.trips.map((trip, id) => {
                        return (
                            <GalleryCard
                                key={id}
                                name={trip.name}
                                image={trip.image}
                                startCity={trip.start_city}
                                endCity={trip.end_destination}
                                updateStartCity={updateStartCity}
                                updateEndCity={updateEndCity}
                                updateJourneys={updateJourneys}
                                currentLat={currentLat}
                                currentLon={currentLon}
                                updateLoading={updateLoading}
                            />
                        )
                    })
                }
            </div>
        </div>
    );
}

export default Gallery;