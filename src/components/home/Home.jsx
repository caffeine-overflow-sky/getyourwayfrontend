import React from "react";
import GetYourWayLogo from "../../common/logos/getyourway_white.png";
import Search from "../search/Search.jsx";
import "./Home.css";
import PropTypes from "prop-types";
import Gallery from "./gallery/Gallery";
import ArrowSVG from "../../common/icon/arrow";

const Home = (props) => {
  let {
    searchClicked,
    currentLat,
    currentLon,
    startCity,
    endCity,
    updateStartCity,
    updateEndCity,
    updateLoading,
    currentUser,
    updateJourneys,
    updateSearchClicked
  } = props;

  return (
    <>
      <div className="section-home d-flex flex-column">
        <div className="overlay d-flex flex-column">
          <header className="section-home-content pb-5">
            <img
              className="logo img-fluid mx-auto d-block my-3"
              src={GetYourWayLogo}
              alt="getyourway logo"
            />
            <p className="quote text-center h1 cssanimation hu__hu__ sequence">{`Welcome ${currentUser.username}! `}</p>
            <p className="quote text-center h5 cssanimation hu__hu__ sequence">
              Ready to start your adventure?
            </p>
          </header>
          <Search
            type="home"
            currentLat={currentLat}
            currentLon={currentLon}
            startCity={startCity}
            endCity={endCity}
            updateStartCity={updateStartCity}
            updateEndCity={updateEndCity}
            updateLoading={updateLoading}
            updateJourneys={updateJourneys}
            searchClicked={searchClicked}
            updateSearchClicked={updateSearchClicked}
          />
          <div className="arrowcontainer">
            <a href="#gallery" className="homeArrow">
              <ArrowSVG />
            </a>
          </div>
        </div>
      </div>
      <Gallery
        updateStartCity={updateStartCity}
        updateEndCity={updateEndCity}
        updateJourneys={updateJourneys}
        currentLat={currentLat}
        currentLon={currentLon}
        updateLoading={updateLoading}
      />
    </>
  );
};

Home.propTypes = {
  currentUser: PropTypes.object.isRequired,
};

Home.defaultProps = {
  currentUser: { username: "username" },
};

export default Home;
