import React from "react";
import { Link } from "react-router-dom";
import AuthService from "../../services/auth.service";
import PropTypes from "prop-types";
import GetYourWayLogo from "../../common/logos/getyourway.png";
import "./Navbar.css";

const Navbar = ({ currentUser, getUser }) => {
  const logOut = () => {
    AuthService.logout();
    getUser();
  };

  return (
    <div>
      {currentUser ? (
        <nav className="navbar navbar-expand-sm navbar-light border-bottom p-1 pr-2 pl-2">
          <Link to="/home" className="navbar-brand">
            <img
              id="getyourwaylogo"
              className="getyouwaylogo img-fluid"
              src={GetYourWayLogo}
              alt="getyourway logo"
            />
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item d-flex align-items-center justify-content-center">
              <Link to="/travel" className="nav-link navbar-text">
                Travel
              </Link>
            </li>
          </div>
          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link
                to="/"
                className="nav-item d-flex align-items-center justify-content-center"
                onClick={logOut}
              >
                <button
                  id="logout"
                  type="button"
                  className="btn btn-outline-primary btn-sm"
                  onClick={logOut}
                >
                  Log Out
                </button>
              </Link>
            </li>
          </div>
        </nav>
      ) : (
          <nav className="navbar navbar-expand-sm navbar-light border-bottom">
            <Link to="/" className="navbar-brand">
              <img
                id="getyourwaylogo"
                className="getyouwaylogo img-fluid"
                src={GetYourWayLogo}
                alt="getyourway logo"
              />
            </Link>
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link
                  to="/"
                  className="nav-item d-flex align-items-center justify-content-center"
                >
                  <button
                    id="login"
                    type="button"
                    className="btn btn-outline-primary btn-sm"
                  >
                    Login
                </button>
                </Link>
              </li>
            </div>
          </nav>
        )}
    </div>
  );
};

Navbar.propTypes = {
  currentUser: PropTypes.shape({
    username: PropTypes.string
  }),
};

Navbar.defaultProps = {
  currentUser: null,
};
export default Navbar;
