import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import AuthService from "../../services/auth.service";
import cities from "../utils/cities";
import "../search/Search.css";

const Search = (props) => {
  let {
    type,
    searchClicked,
    journeys,
    currentLat,
    currentLon,
    startCity,
    endCity,
    updateStartCity,
    updateEndCity,
    updateLoading,
    updateJourneys,
    updateSearchClicked
  } = props;

  const [startCitiesToDisplay, setStartCitiesToDisplay] = useState([]);
  const [endCitiesToDisplay, setEndCitiesToDisplay] = useState([]);

  let history = useHistory();

  let errorMessage = !searchClicked ?
    "" :
    journeys && journeys.departureCity ?
      "" :
      journeys && !journeys.departureCity ?
        "Oops! Looks like something went wrong on our end. Try again..." :
        "DREAM ON, TRY AGAIN IN 2021.. ";

  const handleCityInput = (e, cityType) => {
    let inputtedCity = e.target.value;
    let newCities = [];

    cities
      .filter((city) => city.toLowerCase().includes(inputtedCity.toLowerCase()))
      .map((city, index) => {
        return index < 10 ? newCities = [...newCities, city] : null;
      });
    if (!inputtedCity) newCities = [];
    cityType === "Start City"
      ? updateStartCity(inputtedCity)
      : updateEndCity(inputtedCity);
    cityType === "Start City"
      ? setStartCitiesToDisplay(newCities)
      : setEndCitiesToDisplay(newCities);
  };

  const selectCityOption = (city, cityType) => {
    cityType === "Start City" ? updateStartCity(city) : updateEndCity(city);
    setStartCitiesToDisplay([]);
    setEndCitiesToDisplay([]);
  };

  const handleSearch = async () => {


    updateSearchClicked(true);

    updateJourneys(null);

    try {
      updateLoading(true);
      const journeys = await AuthService.searchJourneys(
        startCity,
        endCity,
        currentLat,
        currentLon
      );

      journeys && updateLoading(false);
      updateJourneys(journeys);
      history.push("/travel");
    } catch (e) {
      updateLoading(false);
    }
  };

  return (
    <>
      <div className={`searchbox d-flex align-items-center justify-content-center ${type ? "flex-column" : "travelsearch "}`}>
        <div className="searchpair p-2">
          <div className="md-form m-2">
            <input
              className="form-control input-box"
              value={startCity}
              type="text"
              placeholder="Start Destination"
              aria-label="Search"
              onChange={(e) => handleCityInput(e, "Start City")}
            />
            <div className="cities">
              {startCitiesToDisplay.map((city, index) => {
                return (
                  <p
                    key={index}
                    onClick={(e) =>
                      selectCityOption(e.target.innerHTML, "Start City")
                    }
                  >
                    {city}
                  </p>
                );
              })}
            </div>
          </div>
          <div className="md-form m-2">
            <input
              className="form-control input-box"
              value={endCity}
              type="text"
              placeholder="End Destination"
              aria-label="Search"
              onChange={(e) => handleCityInput(e, "End City")}
            />
            <div className="cities">
              {endCitiesToDisplay.map((city, index) => {
                return (
                  <p
                    key={index}
                    onClick={(e) =>
                      selectCityOption(e.target.innerHTML, "End City")
                    }
                  >
                    {city}
                  </p>
                );
              })}
            </div>
          </div>
        </div>
        {errorMessage ? <div className="alert alert-warning">{errorMessage}</div> : null}
        <button
          type="button"
          className="orange-button btn btn-secondary btn-sm"
          onClick={() => handleSearch()}
        >
          SEARCH
        </button>
      </div>
    </>
  );
};

export default Search;
