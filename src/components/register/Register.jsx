import React, { useState, useRef } from "react";
import { useHistory } from "react-router-dom";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import AuthService from "../../services/auth.service";
import ValidationServiceHelpers from "../../services/validation.serviceHelpers";

import "./Register.css";

const Register = ({ updateLoading, getUser }) => {
  const form = useRef();
  const checkBtn = useRef();
  let history = useHistory();

  const [username, setUsername] = useState(``);
  const [password, setPassword] = useState(``);
  const [successful, setSuccessful] = useState(false);
  const [message, setMessage] = useState(``);

  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const handleRegister = async (e) => {
    e.preventDefault();

    setMessage(``);
    setSuccessful(false);
    updateLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      const register = await AuthService.register(username, password);
      if (register.message) {
        const login = await AuthService.login(username, password);
        if (localStorage.getItem("user")) {
          getUser();
          setMessage(register.message);
          setSuccessful(true);
          updateLoading(false);
          history.push(`/home`);
          window.location.reload();
        } else {
          console.dir(login);
        }
      } else {
        setMessage(register.error);
        setSuccessful(false);
        updateLoading(false);
      }
    }
  };

  return (
    <div className="section-signup col-md-12">
      <div className="overlay">
        <div className="card card-container">
          <div>
            <p className="medium-text text-center text-break">
              Create an account
              </p>
            <p className="cssanimation hu__hu__ sequence heading-text text-center text-break">
              Get ready for an adventure
              </p>
          </div>

          <Form onSubmit={handleRegister} ref={form}>
            {!successful && (
              <div>
                <div className="form-group">
                  <Input
                    className="form-control"
                    type="text"
                    name="username"
                    placeholder="Create username"
                    value={username}
                    onChange={onChangeUsername}
                    validations={[
                      ValidationServiceHelpers.required,
                      ValidationServiceHelpers.vusername,
                    ]}
                  />
                </div>
                <div className="form-group">
                  <Input
                    className="form-control"
                    type="password"
                    name="password"
                    placeholder="Create password"
                    value={password}
                    onChange={onChangePassword}
                    validations={[
                      ValidationServiceHelpers.required,
                      ValidationServiceHelpers.vpassword,
                    ]}
                  />
                </div>
                <div className="form-group">
                  <button className="btn btn-primary btn-block">
                    Sign Up
                    </button>
                </div>
              </div>
            )}
            {message && (
              <div className="form-group">
                <div
                  className={
                    successful ? `alert alert-success` : `alert alert-danger`
                  }
                  role="alert"
                >
                  {message}
                </div>
              </div>
            )}
            <CheckButton style={{ display: "none" }} ref={checkBtn} />
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Register;
