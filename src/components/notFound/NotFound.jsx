import React from 'react';

const NotFound = () => {
  return (
    <div className="d-flex flex-column align-items-center m-5">
      <h1 className="text-center">Unavailable page</h1>
      <p className="text-center">Sorry, this page does not exist</p>
    </div>
  );
};

export default NotFound;