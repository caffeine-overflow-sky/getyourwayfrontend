import React from "react";
import { create } from "react-test-renderer";
import { BrowserRouter as Router } from "react-router-dom";

import NotFound from "../components/notFound/NotFound";

describe("Not found", () => {
  
  test(`Not found matches snapshot`, () => {
    const notFound = create(
      <Router>
        <NotFound />
      </Router>
    );
    expect(notFound.toJSON()).toMatchSnapshot();
  });
});

