import React from "react";
import { create } from "react-test-renderer";

import Home from "../components/home/Home";

xdescribe("Home", () => {
  test(`Home matches snapshot`, () => {
    const home = create(<Home />);
    expect(home.toJSON()).toMatchSnapshot();
  });

  test(`it should render with the default props`, () => {
    const testRenderer = create(<Home />);
    const testInstance = testRenderer.root;
    const renderedObject = testInstance.findAllByType(`p`);
    expect(renderedObject[0].children).toEqual([
      "Welcome  username! ",
    ]);
  });

  test(`it should render with the correct props`, () => {
    const testObject = { username: "test123" };
    const testRenderer = create(<Home currentUser={testObject} />);
    const testInstance = testRenderer.root;
    const renderedObject = testInstance.findAllByType(`p`);
    expect(renderedObject[0].children).toEqual([
      "Welcome " + testObject.username + "! ",
    ]);
  });
});
