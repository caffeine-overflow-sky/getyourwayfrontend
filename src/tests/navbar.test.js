import React from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom'
import { act, create } from 'react-test-renderer'

import Navbar from '../components/navbar/Navbar';

describe('Navbar renders correctly', () => {
    let mockGetUser;
    let logout;
    let currentUser;
    let testRenderer;
    let testInstance;

    beforeEach(() => {
        mockGetUser = jest.fn();
        logout = jest.fn(() => {
            return mockGetUser;
        });
        currentUser = null;
        testRenderer = create(<Router><Navbar currentUser={currentUser} getUser={mockGetUser} /></Router>);
        testInstance = testRenderer.root;
    });

    afterEach(() => {
        currentUser = null;
        testRenderer = null;
        testInstance = null;
        mockGetUser.mockClear();
        logout.mockClear();
    });

    test("Navbar matches snapshot", () => {
        expect(testRenderer.toJSON()).toMatchSnapshot();
    });

    test("Navbar PropTypes are defined", () => {
        expect(Navbar.propTypes.currentUser).toBeDefined()
    })

    describe("Navbar conditionally renders correctly", () => {
        test("when currentUser prop is null, navbar conditionally renders correctly", () => {
            const link = testInstance.findAllByType(Link);
            expect(link.length).toEqual(2);

            expect(link[0].findByProps({ to: "/" })).toBeTruthy();

            const loginBtn = testInstance.findByType("button");
            expect(loginBtn.children).toContain("Login")

        });

        test("when user is logged in, currentUser prop is defined and navbar conditionally renders correctly", () => {
            currentUser = { username: "test Username" };
            testRenderer = create(<Router><Navbar currentUser={currentUser} getUser={mockGetUser} /></Router>);
            testInstance = testRenderer.root;

            const link = testInstance.findAllByType(Link);
            expect(link.length).toEqual(3);

            expect(link[0].findByProps({ to: "/home" })).toBeTruthy();
            expect(link[1].findByProps({ to: "/travel" })).toBeTruthy();
            expect(link[2].findByProps({ to: "/" })).toBeTruthy();

            const logOutBtn = testInstance.findByType("button");
            expect(logOutBtn.children).toContain("Log Out")
        });
    });

    describe("Navbar logout functionality", () => {
        test("when user is logged in and clicks on the logout button, it logs the user out", () => {
            currentUser = { username: "test Username" };
            testRenderer = create(<Router><Navbar currentUser={currentUser} getUser={mockGetUser} /></Router>);
            testInstance = testRenderer.root;

            const logOutBtn = testInstance.findByType("button");

            act(() => {
                logOutBtn.props.onClick(logout());
            });

            expect(logout).toHaveBeenCalledTimes(1);
            expect(mockGetUser).toHaveBeenCalledTimes(1);
        })
    })
});
