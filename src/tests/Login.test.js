import React from "react";
import { create } from "react-test-renderer";
import { BrowserRouter as Router } from "react-router-dom";

import Login from "../components/login/Login";

xdescribe("Login", () => {
  test(`Login matches snapshot`, () => {
    const login = create(
      <Router>
        <Login />
      </Router>
    );
    expect(login.toJSON()).toMatchSnapshot();
  });
});

