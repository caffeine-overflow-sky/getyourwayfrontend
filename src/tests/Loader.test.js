import React from 'react';
import { create } from "react-test-renderer";
import Loader from "../components/loader/loader";

xdescribe("Loader component renders correctly", () => {
    test("loader component matches snapshot", () => {
        const loader = create(<Loader />);
        expect(loader.toJSON()).toMatchSnapshot();
    });
});