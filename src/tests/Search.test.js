import React from 'react';
import { create } from "react-test-renderer";
import Search from "../components/search/Search";

xdescribe("Search component renders correctly", () => {
    test("Search component matches snapshot", () => {
        const loader = create(<Search />);
        expect(loader.toJSON()).toMatchSnapshot();
    });
});